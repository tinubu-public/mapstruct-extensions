/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.mapstruct.accessornaming;

import static java.beans.Introspector.decapitalize;

import javax.lang.model.element.ExecutableElement;

import org.mapstruct.ap.spi.DefaultAccessorNamingStrategy;

import com.tinubu.commons.lang.beans.AlternativeAccessor;

public class AlternativeAccessorNamingStrategy extends DefaultAccessorNamingStrategy {

   @Override
   public boolean isGetterMethod(ExecutableElement method) {
      return AlternativeAccessor.isGetterMethod(method, elementUtils);
   }

   @Override
   public boolean isSetterMethod(ExecutableElement method) {
      return AlternativeAccessor.isSetterMethod(method, elementUtils);
   }

   @Override
   public boolean isPresenceCheckMethod(ExecutableElement method) {
      return !AlternativeAccessor.transientAccessor(method) && super.isPresenceCheckMethod(method);
   }

   @Override
   public String getPropertyName(ExecutableElement getterOrSetterMethod) {
      if (isPresenceCheckMethod(getterOrSetterMethod)) {
         return decapitalize(getterOrSetterMethod.getSimpleName().toString().substring(3));
      } else {
         return AlternativeAccessor.propertyName(getterOrSetterMethod, elementUtils);
      }
   }

}
